import { Auth, USER_HEADER, ANONYMOUS_HEADER } from "../utils/auth";
import { gql } from "../utils/gql";
import { cleanDatabase } from "../utils/db";
import { path } from "ramda";

describe("createUser", () => {
  const auth = new Auth(8081);

  beforeAll(async () => {
    await auth.start();
  });

  afterAll(async () => {
    await cleanDatabase();
    await auth.stop();
  });

  beforeEach(async () => {
    await cleanDatabase();
  });

  test("anonymous role cannot create user", async () => {
    auth.setResponse(ANONYMOUS_HEADER);

    const res = gql(`mutation {
  insert_user(objects: {display_name: "איתי אדרי", email: "itai@mail.com"}) {
    returning {
      display_name
      email
      id
    }
  }
}`);

    expect(res).rejects.toThrow("no mutations exist");
  });

  test("user role can create user", async () => {
    auth.setResponse(USER_HEADER);

    const res = await gql(`mutation {
  insert_user(objects: {display_name: "איתי אדרי", email: "itai@mail.com"}) {
    returning {
      display_name
      email
      id
    }
  }
}`).then(path(["insert_user", "returning", 0]));

    expect(res).toEqual({
      id: "12345",
      display_name: "איתי אדרי",
      email: "itai@mail.com"
    });
  });

  test("user cannot set id", async () => {
    auth.setResponse(USER_HEADER);

    const res = gql(`mutation {
  insert_user(objects: {display_name: "איתי אדרי", email: "itai@mail.com", id: "1234"}) {
    returning {
      display_name
      email
      id
    }
  }
}`);

    expect(res).rejects.toThrow('field "id" not found in type');
  });
});
