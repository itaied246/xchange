import {
  Auth,
  USER_HEADER,
  ANONYMOUS_HEADER,
  createUserHeader
} from "../utils/auth";
import { gql } from "../utils/gql";
import { cleanDatabase, insert } from "../utils/db";
import { path } from "ramda";

describe("updateUser", () => {
  const auth = new Auth(8081);
  const user = {
    id: "12345",
    display_name: "Itai Edri",
    email: "itai@mail.com"
  };

  beforeAll(async () => {
    await auth.start();
  });

  afterAll(async () => {
    await cleanDatabase();
    await auth.stop();
  });

  beforeEach(async () => {
    await cleanDatabase();
    await insert("user", user);
  });

  test("user cannot update its id", () => {
    auth.setResponse(USER_HEADER);

    const res = gql(`mutation {
        update_user(_set: {id: "00000"}, where: {id: {_eq: "12345"}}) {
          returning {
            id
          }
        }
      }
      `);

    expect(res).rejects.toThrow('field "id" not found in type');
  });

  test("user can update its own data", async () => {
    auth.setResponse(USER_HEADER);

    const res = await gql(`mutation {
        update_user(_set: {display_name: "newName" email: "new@mail.com"}, where: {id: {_eq: "12345"}}) {
          returning {
            display_name
            email
          }
        }
      }
      `).then(path(["update_user", "returning", 0]));

    expect(res).toEqual({
      display_name: "newName",
      email: "new@mail.com"
    });
  });

  test("user cannot update other user's data", async () => {
    auth.setResponse(createUserHeader("00000"));

    const res = await gql(`mutation {
        update_user(_set: {display_name: "newName"}, where: {id: {_eq: "12345"}}) {
          returning {
            display_name
          }
        }
      }
      `).then(path(["update_user", "returning"]));

    expect(res).toHaveLength(0);
  });
});
