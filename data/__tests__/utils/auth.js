import express from "express";

export const createUserHeader = id => ({
  "x-hasura-user-id": id,
  "x-hasura-role": "user"
});

export const ANONYMOUS_HEADER = { "x-hasura-role": "anonymous" };
export const USER_HEADER = createUserHeader("12345");

export class Auth {
  constructor(port) {
    this.port = port;

    this.server = express();

    this.server.get("/verify", (_req, res) => res.json(this.response));
  }

  setResponse(response) {
    this.response = response;
  }

  start() {
    return new Promise((resolve, reject) => {
      this.server = this.server.listen(this.port, err => {
        if (err) reject(err);
        else resolve();
      });
    });
  }

  stop() {
    return new Promise((resolve, reject) => {
      this.server.close(err => {
        if (err) reject(err);
        else resolve();
      });
    });
  }
}
