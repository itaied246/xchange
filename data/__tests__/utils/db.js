import knex from "knex";

const tables = ["user"];

const createConnection = () =>
  knex({
    client: "pg",
    connection: "postgres://postgres:@localhost:5432/postgres"
  });

export const cleanDatabase = async () => {
  const pg = createConnection();

  await Promise.all(tables.map(pg.truncate));
  await pg.destroy();
};

export const insert = async (table, data) => {
  const pg = createConnection();

  await pg(table).insert(data);

  await pg.destroy();
};
