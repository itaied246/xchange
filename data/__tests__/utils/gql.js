import { request } from "graphql-request";

export const gql = query => request("http://localhost:8080/v1/graphql", query);
