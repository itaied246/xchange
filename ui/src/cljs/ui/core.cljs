(ns ui.core
  (:require
   [reagent.core :as reagent]
   [re-frame.core :as re-frame]
   [ui.events :as events]
   [ui.config :as config]
   [ui.routes.core :as routes]
   [ui.auth.core :as auth]))

(defn dev-setup []
  (when config/debug?
    (println "dev mode")))

(defn ^:dev/after-load mount-root []
  (re-frame/clear-subscription-cache!)
  (auth/firebase-init)
  (routes/init-routes!)
  (reagent/render [routes/router-component {:router routes/router}]
                  (.getElementById js/document "app")))

(defn init []
  (re-frame/dispatch-sync [::events/initialize-db])
  (dev-setup)
  (mount-root))
