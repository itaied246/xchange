(ns ui.auth.subs
  (:require
   [re-frame.core :as re-frame]))

(re-frame/reg-sub
 :auth/current-user
 (fn [db _]
   (:current-user db)))