(ns ui.auth.gql)

(def get-profile "query ($id: String!) {
  user_by_pk(id: $id) {
    id
    email
    display_name
  }
}")

(def create-user "mutation ($email: String, $display_name: String) {
  insert_user(objects: {email: $email, display_name: $display_name}) {
    returning {
      id
      email
      display_name
    }
  }
}")