(ns ui.auth.events
  (:require
   [re-frame.core :as re-frame]
   [re-graph.core :as re-graph]))

(defn- parse-user
  [user]
  {:id (:id user)
   :name (:display_name user)
   :email (:email user)})

(defn- create-fb-user
  [user]
  {:id (goog.object/get user "uid")
   :name (goog.object/get user "displayName")
   :email (goog.object/get user "email")})

(re-frame/reg-event-fx
 :auth/login
 (fn []
   {:auth/login nil}))

(re-frame/reg-event-fx
 :auth/logout
 (fn []
   {:auth/logout nil}))

(re-frame/reg-event-fx
 :auth/user-not-authenticated
 (fn []
   {:dispatch-n (list [::re-graph/init
                       {:http-url "http://localhost:8080/v1/graphql"
                        :ws-url nil}]
                      [:auth/set-current-user nil])}))

(re-frame/reg-event-fx
 :auth/user-authenticated
 (fn [{:keys [db]} [_ user]]
   {:auth/user-authenticated user
    :db (assoc db :current-fb-user (create-fb-user user))}))

(re-frame/reg-event-fx
 :auth/get-profile
 (fn [{:keys [db]}]
   {:auth/get-profile (get-in db [:current-fb-user :id])}))

(re-frame/reg-event-fx
 :auth/get-profile-result
 (fn [{:keys [db]} [_ res]]
   (let [user (get-in res [:data :user_by_pk])
         fb-user (:current-fb-user db)]
     (if (nil? user)
       {:auth/create-user [(:email fb-user) (:name fb-user)]}
       {:dispatch-n (list [:auth/set-current-user (parse-user user)]
                          [:route/navigate :pages/home])}))))

(re-frame/reg-event-fx
 :auth/create-user-success
 (fn [_ [_ res]]
   (let [user (get-in res [:data :insert_user :returning 0])]
     {:dispatch-n (list [:auth/set-current-user (parse-user user)]
                        [:route/navigate :pages/profile])})))

(re-frame/reg-event-db
 :auth/set-current-user
 (fn [db [_ user]]
   (assoc db :current-user user)))
