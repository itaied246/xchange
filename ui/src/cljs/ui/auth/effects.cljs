(ns ui.auth.effects
  (:require
   [re-frame.core :as re-frame]
   ["firebase/app" :as firebase]
   [re-graph.core :as re-graph]
   [ui.auth.gql :as gql]))

(defn handle-error
  [error]
  (println "error" error))

(re-frame/reg-fx
 :auth/user-authenticated
 (fn [^js user]
   (-> user
       (.getIdToken)
       (.then #(do (re-frame/dispatch [::re-graph/init
                                       {:http-url "http://localhost:8080/v1/graphql"
                                        :ws-url nil
                                        :http-parameters {:headers {"Authorization" (str "Bearer " %)}}}])
                   (re-frame/dispatch [:auth/get-profile]))))))

(re-frame/reg-fx
 :auth/get-profile
 (fn [id]
   (re-frame/dispatch [::re-graph/query
                       gql/get-profile
                       {:id id}
                       [:auth/get-profile-result]])))

(re-frame/reg-fx
 :auth/create-user
 (fn [[email name]]
   (re-frame/dispatch [::re-graph/mutate
                       gql/create-user
                       {:email email
                        :display_name name}
                       [:auth/create-user-success]])))

(re-frame/reg-fx
 :auth/login
 (fn []
   (-> (firebase/auth) (.signInWithPopup (firebase/auth.GoogleAuthProvider.)) (.catch handle-error))))

(re-frame/reg-fx
 :auth/logout
 (fn []
   (-> (firebase/auth) (.signOut) (.catch handle-error))))
