(ns ui.auth.core
  (:require
   ["firebase/app" :as firebase]
   ["firebase/auth"]
   [re-frame.core :as re-frame]
   [ui.auth.events]
   [ui.auth.effects]
   [ui.auth.subs]))

(defn h
  [user]
  (if (nil? user)
    (re-frame/dispatch [:auth/user-not-authenticated])
    (re-frame/dispatch [:auth/user-authenticated user])))

(defn firebase-init
  []
  (println "initializing firebase")
  (firebase/initializeApp
   #js {:apiKey      "AIzaSyDBK-FKFV8uez-epOza0Ivzjg4KpXBuoZ8"
        :authDomain  "xchange-254811.firebaseapp.com"
        :projectId   "xchange-254811"})
  (-> (firebase/auth) (.onAuthStateChanged h)))