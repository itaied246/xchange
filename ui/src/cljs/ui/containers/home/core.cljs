(ns ui.containers.home.core
  (:require
   [re-frame.core :as rf]))

(defn home []
  (let [user (rf/subscribe [:auth/current-user])]
    (fn [_]
      [:div
       [:h1 "Hello " (get @user :name "anonymous") ", welcome to firebase app !"]
       [:button
        {:on-click #(rf/dispatch [:route/navigate :pages/login])}
        "login-page"]])))