(ns ui.containers.login.core
  (:require
   [re-frame.core :as rf]))

(defn login []
  (let [current-user (rf/subscribe [:auth/current-user])]
    (fn [_]
      [:div
       [:h1 "login page"]
       [:button
        {:on-click #(rf/dispatch [:route/navigate :pages/home])}
        "home"]
       (when-not @current-user
         [:button
          {:on-click #(rf/dispatch [:auth/login])}
          "login"])
       (when @current-user
         [:button
          {:on-click #(rf/dispatch [:auth/logout])}
          "logout"])])))