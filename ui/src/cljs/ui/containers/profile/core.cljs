(ns ui.containers.profile.core
  (:require
   [re-frame.core :as rf]))

(defn profile []
  (let [user (rf/subscribe [:auth/current-user])]
    (fn [_]
      [:div
       [:h1 "Hello " (get @user :name)]])))