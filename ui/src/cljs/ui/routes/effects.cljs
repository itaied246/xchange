(ns ui.routes.effects
   (:require
    [reitit.frontend.easy :as rfe]
    [re-frame.core :as re-frame]))

(re-frame/reg-fx
 :route/navigate
 (fn [route]
   (apply rfe/push-state route)))