(ns ui.routes.core
  (:require
   [re-frame.core :as re-frame]
   [reitit.frontend :as rf]
   [reitit.frontend.easy :as rfe]
   [ui.routes.effects]
   [ui.routes.events]
   [ui.routes.subs]
   [ui.containers.home.core :as home]
   [ui.containers.login.core :as login]
   [ui.containers.profile.core :as profile]))

(def routes
  ["/"
   [""
    {:name      :pages/home
     :view      home/home
     :link-text "Home"
     :controllers
     [{:start (fn [& params] (js/console.log "Entering home page"))
       :stop  (fn [& params] (js/console.log "Leaving home page"))}]}]
   ["login"
    {:name      :pages/login
     :view      login/login
     :link-text "Login"
     :controllers
     [{:start (fn [& params] (js/console.log "Entering login page"))
       :stop  (fn [& params] (js/console.log "Leaving login page"))}]}]
   ["profile"
    {:name      :pages/profile
     :view      profile/profile
     :link-text "Profile"
     :controllers
     [{:start (fn [& params] (js/console.log "Entering profile page"))
       :stop  (fn [& params] (js/console.log "Leaving profile page"))}]}]])

(defn on-navigate [new-match]
  (when new-match
    (re-frame/dispatch [:route/navigated new-match])))

(def router
  (rf/router
   routes))

(defn init-routes! []
  (js/console.log "initializing routes")
  (rfe/start!
   router
   on-navigate
   {:use-fragment false}))

(defn router-component [{:keys [router]}]
  (let [current-route @(re-frame/subscribe [:route/current-route])]
    [:div
     (when current-route
       [(-> current-route :data :view)])]))