(ns ui.routes.events
  (:require
   [re-frame.core :as re-frame]
   [reitit.frontend.controllers :as rfc]))

(re-frame/reg-event-fx
 :route/navigate
 (fn [_ [_ & route]]
   {:route/navigate route}))

(re-frame/reg-event-db
 :route/navigated
 (fn [db [_ new-match]]
   (let [old-match   (:current-route db)
         controllers (rfc/apply-controllers (:controllers old-match) new-match)]
     (assoc db :current-route (assoc new-match :controllers controllers)))))