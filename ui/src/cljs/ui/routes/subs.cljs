(ns ui.routes.subs
  (:require
   [re-frame.core :as re-frame]))

(re-frame/reg-sub
 :route/current-route
 (fn [db]
   (:current-route db)))