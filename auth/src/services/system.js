import { Auth } from "./auth";
import { Server } from "./server";
import { logInfo } from "../logger";

export class System {
  constructor(port, cert) {
    this.auth = new Auth(cert);
    this.server = new Server(this.auth, port);
  }

  async start() {
    this.auth.start();
    await this.server.start();
    logInfo("system is running");
  }

  async stop() {
    await this.server.stop();
  }
}
