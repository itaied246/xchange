import express from "express";
import { verifyHandler } from "../auth/handlers";
import { logInfo } from "../logger";

export class Server {
  constructor(auth, port) {
    this.port = port;
    this.server = express();

    this.server.get("/verify", verifyHandler(auth));
  }

  start() {
    return new Promise((resolve, reject) => {
      this.server = this.server.listen(this.port, err => {
        if (err) reject(err);
        else {
          logInfo("server is listening on " + this.port);
          resolve();
        }
      });
    });
  }

  stop() {
    return new Promise((resolve, reject) => {
      this.server.close(err => {
        if (err) reject(err);
        else resolve();
      });
    });
  }
}
