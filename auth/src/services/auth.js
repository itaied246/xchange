import * as admin from "firebase-admin";
import { logInfo } from "../logger";

export class Auth {
  constructor(cert) {
    this.cert = cert;
  }

  start() {
    admin.initializeApp({
      credential: admin.credential.cert(JSON.parse(this.cert))
    });
    logInfo("firebase initialized");
  }

  verify(token) {
    return admin.auth().verifyIdToken(token);
  }
}
