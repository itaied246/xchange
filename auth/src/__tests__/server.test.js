import { Server } from "../services/server";
import axios from "axios";

describe("server", () => {
  let auth, server;
  const uid = "123";
  const port = 9000;
  const ROUTE = `http://localhost:${port}/verify`;

  beforeAll(() => {
    auth = {
      verify: jest.fn(token =>
        token === "valid"
          ? Promise.resolve({ uid })
          : Promise.reject("mocked error")
      )
    };
    server = new Server(auth, port);
  });

  beforeEach(async () => {
    await server.start();
  });

  afterEach(async () => {
    await server.stop();
  });

  test("request without token should return anonymous", async () => {
    const res = await axios.get(ROUTE);

    expect(res.data).toEqual({ "x-hasura-role": "anonymous" });
  });

  test("request with invalid token should return anonymous", async () => {
    const res = await axios.get(ROUTE, {
      headers: { authorization: "Bearer invalid" }
    });

    expect(res.data).toEqual({ "x-hasura-role": "anonymous" });
  });

  test("request with valid token should return user", async () => {
    const res = await axios.get(ROUTE, {
      headers: { authorization: "Bearer valid" }
    });

    expect(res.data).toEqual({
      "x-hasura-role": "user",
      "x-hasura-user-id": uid
    });
  });
});
