import { path, head, nth, pipe, equals, split, not, isNil } from "ramda";
import { logError } from "../logger";

const HEADERS = {
  ROLE: "x-hasura-role",
  ROLE_USER: "user",
  ROLE_ANON: "anonymous",
  USER: "x-hasura-user-id"
};

export const verifyHandler = auth => async (req, res) => {
  const token = getToken(req);

  if (token) {
    try {
      const { uid } = await auth.verify(token);
      res.json({ [HEADERS.USER]: uid, [HEADERS.ROLE]: HEADERS.ROLE_USER });
    } catch (e) {
      logError(e);
      res.json({ [HEADERS.ROLE]: HEADERS.ROLE_ANON });
    }
  } else {
    res.json({ [HEADERS.ROLE]: HEADERS.ROLE_ANON });
  }
};

const getAuthorizationHeader = path(["headers", "authorization"]);

const isBearer = pipe(
  split(" "),
  head,
  equals("Bearer")
);

const bearerToken = pipe(
  split(" "),
  nth(1)
);

const getToken = req => {
  const header = getAuthorizationHeader(req);
  let result = null;

  if (not(isNil(header)) && isBearer(header)) {
    result = bearerToken(header);
  }

  return result;
};
