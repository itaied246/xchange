import { validate } from "./config/schema";
import { System } from "./services/system";
import { logError, logInfo } from "./logger";

const main = async env => {
  validate(env);

  const system = new System(env.PORT, env.FIREBASE_CONFIG);

  await system.start();
};

main(process.env)
  .catch(logError);
