import log4js from "log4js";

const logger = log4js.getLogger();
logger.level = "info";

export const logError = e => logger.error(e);

export const logInfo = l => logger.info(l);
