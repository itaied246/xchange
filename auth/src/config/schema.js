import joi from "@hapi/joi";

const schema = joi
  .object({
    PORT: joi.string().default("8080"),
    FIREBASE_CONFIG: joi.string().required()
  })
  .unknown();

export const validate = env => {
  try {
    joi.assert(env, schema);
  } catch (e) {
    throw new Error(e.details);
  }
};
