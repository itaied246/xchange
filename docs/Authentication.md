# Authentication

The process of authenticating a user.

## Flow

### User is logged out

#### Story

- User clicks `login` button
- User select login method (Google, Facebook ...)
- If user doesn't exist:
  - Load registration page
  - User may update and click finish
- User gets back to home screen

#### Implementation

- User perform `firebase` authentication
- Token is sent to backend with `get_profile` request
- If user doesn't exist:
  - Send `create_user` request
  - Display registration page (name, email ...)
  - If user update its data, send `update_user` request
- Load profile
- Load home screen

### User is logged in

#### Story

- User opens arbitrary page
- User is being authenticated in the background

#### Implementation

- Token is being verified
- Token is sent with `get_profile`
- Load profile